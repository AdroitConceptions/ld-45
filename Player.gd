extends KinematicBody2D

export var jumpSpeed = 200
export var moveSpeed = 20
export var gravity = 20

signal hpChange
signal newAbility

var velocity = Vector2()
var available_jump_count = 0
var max_jump = 2
var is_jumping = false

var hp = 3

var upgrades = []

func _ready():
	$AnimatedSprite.playing = true
	$DoubleJump.playing = true
	update_abilities()
	emit_signal("hpChange", hp)

func update_abilities():
	if upgrades.has("DoubleJump"):
		max_jump = 2
	elif upgrades.has("Jump"):
		max_jump = 1
	else:
		max_jump = 0
		
	$Slash.can_attack = upgrades.has("Attack")
	
	if upgrades.has("EndGoal"):
		hp = 0
		velocity = Vector2()
		$AnimatedSprite.play("Idle")

func process_input():
	velocity.x = 0
	if Input.is_action_pressed("ui_left"):
		velocity.x -= moveSpeed
		$AnimatedSprite.flip_h = true
		$Slash.transform = Transform2D.FLIP_X
	if Input.is_action_pressed("ui_right"):
		velocity.x += moveSpeed
		$AnimatedSprite.flip_h = false
		$Slash.transform = Transform2D.IDENTITY
	if is_on_floor():
		available_jump_count = max_jump
		$CayoteTimer.start()
	if available_jump_count > 0 and Input.is_action_just_pressed("ui_accept"):
		is_jumping = true
		if available_jump_count < max_jump:
			$DoubleJump.play("default")
			$DoubleJump.frame = 0
		available_jump_count -= 1
		$JumpTimer.start()
	if Input.is_action_just_released("ui_accept"):
		is_jumping = false
	if is_jumping:
		velocity.y = -jumpSpeed


func set_animation():
	if !is_on_floor():
		$AnimatedSprite.play("Jump")
	elif velocity.x == 0:
		$AnimatedSprite.play("Idle")
	else:
		$AnimatedSprite.play("Runing")
	
# warning-ignore:unused_argument
func _physics_process(delta):
	if hp > 0:
		process_input()
		set_animation()
		
	velocity.y += gravity
	velocity.y = clamp(velocity.y, -jumpSpeed * 2, gravity * 50)
	velocity = move_and_slide(velocity, Vector2.UP, true)

func _on_CayoteTimer_timeout():
	if available_jump_count > 1:
		available_jump_count -= 1

func _on_JumpTimer_timeout():
	is_jumping = false

func take_damage():
	hp -= 1
	if hp >= 0:
		$hurt.play()
		emit_signal("hpChange", hp)
	if hp == 0:
		$AnimatedSprite.play("Die")
		$AnimatedSprite.frame = 0
	
func get_upgrade(upgrade):
	if !upgrades.has(upgrade):
		upgrades.append(upgrade)
		emit_signal("newAbility", upgrade)
		update_abilities()