extends Node2D

func _ready():
	$CanvasLayer/ColorRect/AnimationPlayer.play("FadeToTranspart")
	$CanvasLayer/Quit.visible = false
	$CanvasLayer/StartOver.visible = false
	$CanvasLayer/YouDied.visible = false
	_on_ClearAbilityMessage_timeout()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Player_hpChange(new_hp):
	if new_hp <= 0:
		$CanvasLayer/ColorRect/AnimationPlayer.play("FadeToBlack")

func _on_StartOver_button_down():
# warning-ignore:return_value_discarded
	get_tree().reload_current_scene()

func _on_Quit_button_down():
	get_tree().quit()

func _on_Player_newAbility(ability_name):
	if ability_name == "DoubleJump":
		show_message($CanvasLayer/CanDoubleJump)
	elif ability_name == "Jump":
		show_message($CanvasLayer/CanJump)
	elif ability_name == "Attack":
		show_message($CanvasLayer/CanAttack)
	elif ability_name == "EndGoal":
		$CanvasLayer/YouDied.bbcode_text = "[center]You Won![/center]"
		$CanvasLayer/ColorRect/AnimationPlayer.play("FadeToBlack")

func show_message(ui_item):
	ui_item.visible =  true
	$ClearAbilityMessage.start()

func _on_ClearAbilityMessage_timeout():
	$CanvasLayer/CanAttack.visible = false
	$CanvasLayer/CanJump.visible = false
	$CanvasLayer/CanDoubleJump.visible = false
