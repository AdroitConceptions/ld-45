#!/bin/bash

cd Exports
rm *.zip

zip -r LD45_AdroitConceptions_Windows.zip Windows/
zip -r LD45_AdroitConceptions_Linux.zip Linux/
zip -r LD45_AdroitConceptions_MacOS.zip MacOS/
