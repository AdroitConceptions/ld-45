extends KinematicBody2D

const GRAVITY_VEC = Vector2(0, 900)
const FLOOR_NORMAL = Vector2(0, -1)

const STATE_WALKING = 0
const STATE_KILLED = 1
const WALK_SPEED = 70 

var linear_velocity = Vector2()
var direction = -1

# state machine
var state = STATE_WALKING

onready var DetectFloorLeft = $DetectFloorLeft
onready var DetectWallLeft = $DetectWallLeft
onready var DetectFloorRight = $DetectFloorRight
onready var DetectWallRight = $DetectWallRight

func _ready():
	$AnimatedSprite.play("default")
	$AnimatedSprite.playing = true

func _physics_process(delta):
	if state == STATE_WALKING:
		linear_velocity += GRAVITY_VEC * delta
		linear_velocity.x = direction * WALK_SPEED
		linear_velocity = move_and_slide(linear_velocity, FLOOR_NORMAL)

		if not DetectFloorLeft.is_colliding() or DetectWallLeft.is_colliding():
			direction = 1.0

		if not DetectFloorRight.is_colliding() or DetectWallRight.is_colliding():
			direction = -1.0

func take_damage():
	if state == STATE_WALKING:
		$Squish.play()
		state = STATE_KILLED
		$AnimatedSprite.play("Death")
		$AnimatedSprite.frame = 0
		$CollisionShape2D.disabled = true
		$Area2D/CollisionShape2D.disabled = true

func _on_Area2D_body_entered(body):
	if body != self and state == STATE_WALKING:
		if body.has_method("take_damage"):
			body.call("take_damage")

func _on_AudioStreamPlayer2D_finished():
	queue_free()
