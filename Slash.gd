extends Node2D

export var can_attack = true

func _ready():
	$Sprite.visible = false
	$StrikeArea/CollisionShape2D.disabled = true

# warning-ignore:unused_argument
func _physics_process(delta):
	if can_attack and Input.is_action_just_pressed("attack"):
		$Sprite.visible = true
		$StrikeArea/CollisionShape2D.disabled = false
		$SlashTimer.start()
		$Swish.play()
		
		can_attack = false
		$AttackCooldown.start()

func _on_SlashTimer_timeout():
	$Sprite.visible = false
	$StrikeArea/CollisionShape2D.disabled = true

func _on_AttackCooldown_timeout():
	can_attack = true

func _on_StrikeArea_body_entered(body):
	if body.has_method("take_damage"):
		body.call("take_damage")
