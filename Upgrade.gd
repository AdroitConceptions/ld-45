tool
extends Area2D

export var upgrade_name = ""
export(Texture) var asset setget my_func

func my_func(tex):
    asset = tex
    if Engine.editor_hint:
        get_node("Sprite").texture = asset

func _ready():
	if not Engine.editor_hint:
	    get_node("Sprite").texture = asset

func _on_Upgrade_body_entered(body):
	if not Engine.editor_hint:
		if body.has_method("get_upgrade"):
			body.call("get_upgrade", upgrade_name)
			queue_free()
