extends Node2D

export(Texture) var full_heart
export(Texture) var empty_heart

var last_hp_value = 0

func _ready():
	pass

func hp_update(new_hp):
	set_heart($Sprite, new_hp >= 1)
	set_heart($Sprite2, new_hp >= 2)
	set_heart($Sprite3, new_hp >= 3)
	last_hp_value = new_hp

func set_heart(sprt, full):
	if full:
		sprt.texture = full_heart
	else:
		sprt.texture = empty_heart


func _on_Player_hpChange(new_hp):
	print(new_hp)
	hp_update(new_hp)
